import java.text.DecimalFormat;

public class Task14 {

    // Function for checking if string is a valid number
    public static boolean isNumeric(String str) { 
        try {  
          Double.parseDouble(str);  
          return true;
        } catch(NumberFormatException e){  
          return false;  
        }  
      }

      public static void printInstructions() {
        System.out.println("Please provide arguments in following format: 'java Task14.java <weight in kg> <height in meters>'");
        System.out.println("Example: 'java Task14.java 82.5 1.88'");
            
      }
    
    public static void main( String[] args ) {

        // Check if both arguments are provided
        if (args.length < 2) {
            printInstructions();
            System.exit(0) ;
        }
        // Check if weight argument is a valid number
        else if (!isNumeric(args[0])) {
            System.out.println("Please enter a valid weight");
            printInstructions();
            System.exit(0) ;
        }
        // Check if height argument is a valid number
        else if (!isNumeric(args[1])) {
            System.out.println("Please enter a valid height");
            printInstructions();
            System.exit(0) ;
        }

        try {
            // Calculate BMI:
            double bmi = Double.parseDouble(args[0]) / (Double.parseDouble(args[1]) * Double.parseDouble(args[1]));

            // Set decimalformat to only get 2 decimals
            DecimalFormat doubleFormat = new DecimalFormat("#.00");

            // Print BMI and the correct category
            if (bmi < 18.5)  System.out.println("Your BMI is " + doubleFormat.format(bmi) + " which means that you are underweight.");
            else if (bmi < 24.9)  System.out.println("Your BMI is " + doubleFormat.format(bmi) + " which means that you are normal weight.");
            else if (bmi < 29.0)  System.out.println("Your BMI is " + doubleFormat.format(bmi) + " which means that you are overweight.");
            else if (bmi >= 30)  System.out.println("Your BMI is " + doubleFormat.format(bmi) + " which means that you are obese.");
        }
        // Catch Exceptions 
        catch (Exception e) {
            System.out.println(e.toString());
        }
                
    }
}  